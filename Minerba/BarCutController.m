//
//  BarCutController.m
//  Minerba
//
//  Created by See.Ku on 2014/04/03.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import "sk4xlib.h"
#import "SK4MessageView.h"
#import "SK4BoolStringTransformer.h"

#import "BarCutAdmin.h"
#import "BarCutController.h"

@interface BarCutController ()
{
	BarCutAdmin* _admin;
}
@end

@implementation BarCutController

- (id)init
{
	self = [super init];
	if( self == nil )
		return nil;
	
	//		初期化
	
	[[NSBundle mainBundle] loadNibNamed:@"BasicConvertWindow" owner:self topLevelObjects:nil];

	return self;
}

- (void)awakeFromNib
{
	//		初期化

	NSWindow* win = self.window;
	SK4MessageView* mv = self.messageView;

	self.settingsKey = @"BarCut";
	win.title = @"Cut StatusBar";
	mv.message = @"Please drop the directory\n or iOS screen shot.\n(640x960 - 1536x2048 png)";

	self.inputLabel.stringValue = @"Input File";

	[self.typeLabel setHidden:YES];
	[self.typeCombo setHidden:YES];

	[win setFrameAutosaveName:[self makeKey:@"BarCutWindow"]];

	[super awakeFromNib];
}

- (BOOL)tableView:(NSTableView *)tableView shouldSelectRow:(NSInteger)row
{
	BarCutUnit* unit = [[self.arrayController arrangedObjects] objectAtIndex:row];

	NSImageView* iv = [self drawerImageView];
	iv.image = unit.image;

	return YES;
}

////////////////////////////////////////////////////////////////

- (void)setupTableView
{
	//		テーブルビューを設定

	NSTableView* tv = self.tableView;
	NSArrayController* ac = self.arrayController;
	NSTableColumn* col = nil;

	col = [tv tableColumnWithIdentifier:@"flagColumn"];
	[col bind:@"value" toObject:ac withKeyPath:@"arrangedObjects.convertFlag" options:nil];

	col = [self makeTableColumn:@"Filename" width:280 align:NSLeftTextAlignment editable:YES];
	[col bind:@"value" toObject:ac withKeyPath:@"arrangedObjects.filename" options:nil];
	[tv addTableColumn:col];

	col = [self makeTableColumn:@"Size" width:88 align:NSCenterTextAlignment editable:NO];
	[col bind:@"value" toObject:ac withKeyPath:@"arrangedObjects.sizeString" options:nil];
	[tv addTableColumn:col];

	[self addTableColumn:@"Information" width:200 path:@"info" options:nil];
}

- (void)convert
{
	//		変換処理を実行

	[_admin convert:self.outputDir.stringValue];
}

////////////////////////////////////////////////////////////////

- (void)setupItemArray
{
	//		アイテムの一覧を初期化

	_admin = [[BarCutAdmin alloc] init];
}

- (void)addItemArray:(NSString*)file path:(NSString*)path
{
	//		アイテムの一覧にファイルを追加

	[_admin addFile:file path:path];
}

- (NSArray*)itemArray
{
	//		アイテムの一覧を返す

	return _admin.unitArray;
}

@end

// eof
