//
//  AppIconType.m
//  Minerba
//
//  Created by See.Ku on 2014/03/29.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import "sk4xlib.h"
#import "SK4BitmapContext.h"

#import "AppIconUnit.h"
#import "AppIconAdmin.h"

@interface AppIconUnit ()
{
	SK4BitmapContext* _icon;
}
@end

@implementation AppIconUnit

- (id)init
{
	self = [super init];
	if( self == nil )
		return nil;

	//		初期化

	_convertFlag = YES;

	return self;
}

- (void)setupUse:(NSInteger)use type:(NSInteger)type require:(BOOL)require retina:(BOOL)retina size:(CGSize)size
{
	_use = use;
	_type = type;
	_require = require;
	_retina = retina;
	_size = size;
}

- (void)setupImage:(NSImage*)image filename:(NSString*)fn
{
	//		TODO: 命名規則を指定できるようにする？

	_filename = sk4Printf( @"%@-%d.png", fn, (int)_size.width );

	BOOL alpha = [self hasAlpha];

	_icon = [[SK4BitmapContext alloc] initWithSize:_size alpha:alpha];
	_icon.interpolationQuality = kCGInterpolationHigh;
	[_icon drawImage:image toRect:CGRectZero fromRect:CGRectZero];
}

- (void)createIcon:(NSString*)output
{
	NSString* path = [output stringByAppendingPathComponent:_filename];

	NSData* data = _icon.makePngData;
	[data writeToFile:path atomically:YES];

	self.convertFlag = NO;
}

////////////////////////////////////////////////////////////////

- (BOOL)hasAlpha
{
	if( _type == APP_ICON_TYPE_OSX )
		return YES;
	else
		return NO;
}

- (NSString*)useString
{
	NSString* str[] = {
		@"App Icon",
		@"Spotlight",
		@"Settings",
		@"App Store",
	};

	return str[_use];
}

- (NSString*)typeString
{
	NSString* str[] = {
		@"iPhone",
		@"iPad",
		@"OS X",
	};

	return str[_type];
}

- (NSString*)sizeString
{
	return sk4Printf( @"%gx%g", _size.width, _size.height );
}

- (NSImage*)convertImage
{
	return _icon.makeImage;
}

@end
