//
//  AppIconType.h
//  Minerba
//
//  Created by See.Ku on 2014/03/29.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import <Foundation/Foundation.h>

enum {
	APP_ICON_USE_APP_ICON = 0,
	APP_ICON_USE_SPOTLIGHT,
	APP_ICON_USE_SETTINGS,
	APP_ICON_USE_APP_STORE,

	APP_ICON_USE_MAX
};

enum {
	APP_ICON_TYPE_IPHONE = 0,
	APP_ICON_TYPE_IPAD,
	APP_ICON_TYPE_OSX,

	APP_ICON_TYPE_MAX
};

@class AppIconAdmin;

@interface AppIconUnit : NSObject

@property (nonatomic) NSInteger use;
@property (nonatomic) NSInteger type;
@property (nonatomic) BOOL require;
@property (nonatomic) BOOL retina;
@property (nonatomic) CGSize size;

@property (nonatomic) BOOL convertFlag;
@property (nonatomic) NSString* filename;

- (void)setupUse:(NSInteger)use type:(NSInteger)type require:(BOOL)require retina:(BOOL)retina size:(CGSize)size;
- (void)setupImage:(NSImage*)image filename:(NSString*)fn;
- (void)createIcon:(NSString*)output;

- (BOOL)hasAlpha;
- (NSString*)useString;
- (NSString*)typeString;
- (NSString*)sizeString;
- (NSImage*)convertImage;

@end
