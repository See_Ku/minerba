//
//  AppIconType.m
//  Minerba
//
//  Created by See.Ku on 2014/03/29.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import "sk4xlib.h"
#import "SK4BitmapContext.h"

#import "AppIconType.h"
#import "AppIconAdmin.h"

@interface AppIconType ()
{
	AppIconAdmin* _admin;
	SK4BitmapContext* _icon;
}
@end

@implementation AppIconType

- (id)init
{
	NSLog( @"You drive me crazy." );

	return nil;
}

- (id)initWidthAdmin:(AppIconAdmin*)admin
{
	self = [super init];
	if( self == nil )
		return nil;
	
	//		初期化

	_admin = admin;
	_convertFlag = YES;

	return self;
}

- (void)setupUse:(NSInteger)use type:(NSInteger)type require:(BOOL)require retina:(BOOL)retina size:(CGSize)size
{
	_use = use;
	_type = type;
	_require = require;
	_retina = retina;
	_size = size;

	//		TODO: 命名規則を指定できるようにする？

	_filename = sk4Printf( @"%@-%d.png", _admin.filename, (int)size.width );

	BOOL alpha = NO;
	if( _type == APP_ICON_TYPE_OSX )
		alpha = YES;

	_icon = [[SK4BitmapContext alloc] initWithSize:_size alpha:alpha];
	_icon.interpolationQuality = kCGInterpolationHigh;
	[_icon drawImage:_admin.image toRect:CGRectZero fromRect:CGRectZero];
}

- (void)createIcon
{
	NSString* path = [_admin.outputDir stringByAppendingPathComponent:_filename];

	NSData* data = _icon.makePngData;
	[data writeToFile:path atomically:YES];

	self.convertFlag = NO;
}

- (NSImage*)convertImage
{
	return _icon.makeImage;
}

////////////////////////////////////////////////////////////////

- (NSString*)useString
{
	NSString* str[] = {
		@"App Icon",
		@"Spotlight",
		@"Settings",
		@"App Store",
	};

	return str[_use];
}

- (NSString*)typeString
{
	NSString* str[] = {
		@"iPhone",
		@"iPad",
		@"OS X",
	};

	return str[_type];
}

- (NSString*)sizeString
{
	return sk4Printf( @"%gx%g", _size.width, _size.height );
}

@end
