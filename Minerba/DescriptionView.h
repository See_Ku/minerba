//
//  DescriptionView.h
//  Minerba
//
//  Created by See.Ku on 2014/03/28.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class DrawDescription;

@interface DescriptionView : NSView

@property (nonatomic) DrawDescription* descript;

@end
