//
//  BarCutUnit.h
//  Minerba
//
//  Created by See.Ku on 2014/04/03.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BarCutUnit : NSObject

@property (nonatomic) BOOL convertFlag;
@property (nonatomic) NSString* filename;

@property (nonatomic) CGSize size;
@property (nonatomic) NSString* info;

@property (readonly, nonatomic) NSImage* image;

- (BOOL)readFile:(NSString*)file path:(NSString*)path;
- (BOOL)execFile:(NSString*)outdir;

- (NSString*)sizeString;

@end
