//
//  SK4DropFileView.h
//  	for sk4lib
//
//  Created by See.Ku on 2014/03/13.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import <Cocoa/Cocoa.h>

@protocol SK4DropFileViewDelegate <NSObject>
@optional

/**
 *  ドロップされたファイル・ディレクトリの情報をそのまま受け取る
 *
 *  @param files ドロップされたファイル・ディレクトリの一覧
 */
- (void)dropFileReceived:(NSArray*)files;


/**
 *  ドロップされたファイル・ディレクトリから対象となるファイルの解析を開始
 *
 *  @param files ドロップされたファイル・ディレクトリの一覧
 */
- (void)dropFileParseStart:(NSArray*)files;


/**
 *  解析で対象となるファイルが見つかった
 *
 *  @warning fileにはpath以降のディレクトリ構成を含むことに注意
 *
 *  @param file ファイル名。pathと連結することでフルパスになる
 *
 *  	NSString* full = [path stringByAppendingPathComponent:file];
 *
 *  @param path ドロップの起点となるパス
 */
- (void)dropFileParseFile:(NSString*)file path:(NSString*)path;


/**
 *  ファイルの解析が終了した
 */
- (void)dropFileParseEnd;

@end

////////////////////////////////////////////////////////////////

@interface SK4DropFileView : NSView

@property (nonatomic) id<SK4DropFileViewDelegate> dropDelegate;

/**
 *  YES: ファイルを自力で解析する際、『.』で始まるファイルを対象外とする
 *
 *  デフォルトはYES。
 */
@property (nonatomic) BOOL selectHiddenFiles;


/**
 *  ファイルを自力で解析する際、対象とするファイルの拡張子
 */
@property (nonatomic) NSArray* allowedFileTypes;


/**
 *  ドロップされたファイルを解析する
 *
 *  @param files ドロップされたファイルの一覧
 */
- (void)droppedFileParse:(NSArray*)files;

@end
