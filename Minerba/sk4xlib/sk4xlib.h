//
//  sk4xlib.h
//
//
//  Created by See.Ku on 2014/03/17.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#ifndef __SK4_X_LIB__
#define __SK4_X_LIB__

#ifdef __cplusplus
extern "C" {
#endif

////////////////////////////////////////////////////////////////
/// @name	各種宣言・マクロ

/**
 *  引数なし、戻り値なしのブロック宣言
 */
typedef void (^SK4VoidBlock)(void);


/**
 *  NSString stringWithFormatをラップするマクロ
 *
 *  @param f   フォーマット
 *  @param ... 引数
 *
 *  @return 無し
 */
#define	sk4Printf( f,... ) [NSString stringWithFormat:f, __VA_ARGS__]


/**
 *  staticな文字列定数を簡単に定義するためのマクロ
 *
 *  @param key 文字列定数の識別子。そのまま、文字列の内容にもなる
 *
 *  @return 無し
 */
#define	SK4_DEFINE_STATIC( key )	static NSString* key = @#key


/**
 *  グローバルな文字列定数を簡単に定義するためのマクロ
 *
 *  ヘッダでは、以下の形式で文字列定数を宣言する。
 *  ex)
 *    SK4_DEFINE_TO_REALIZE( AAA_STRING1 );
 *    SK4_DEFINE_TO_REALIZE( AAA_STRING2 );
 *  
 *  ヘッダを読み込んでいるソースファイルのうちの1つで、
 *  ヘッダファイルを読み込む前に『SK4_DEFINE_TO_REALIZE』を
 *  定義しておくことで、文字列定数に実体が生成される。
 *  ex)
 *    #define SK4_DEFINE_TO_REALIZE
 *    #import "ColorPattern.h"
 *  
 *  
 *  @param key 文字列定数の識別子。そのまま、文字列の内容にもなる
 *
 *  @return 無し
 */
#ifdef	SK4_DEFINE_TO_REALIZE
	#define	SK4_DEFINE_EXTERN( key )	NSString* const key = @#key
#else
	#define	SK4_DEFINE_EXTERN( key )	extern NSString* const key
#endif


/**
 *  リリースバージョンでNSLogを無効にするためのマクロ
 *
 *  実際には『NS_BLOCK_ASSERTIONS』が定義されている時に無効になる。
 *
 *  @param f   フォーマット
 *  @param ... 引数
 *
 *  @return 無視
 */
#ifdef NS_BLOCK_ASSERTIONS
	#ifndef NSLog
		#define NSLog( f, args... )
	#endif
#else
	#ifndef NSLog
		#define NSLog( f, args... ) NSLog( f, ##args )
	#endif
#endif



	
	

////////////////////////////////////////////////////////////////
/// @name	文字列関係

/**
 *  文字列が空かどうかを判定する関数
 *
 *  @param str 文字列
 *
 *  @return 空だった場合はYES
 */
BOOL	sk4IsEmptyString( NSString* str );


/**
 *  文字列を比較（nil対応）
 *
 *  @param str1 文字列1
 *  @param str2 文字列2
 *
 *  @return 一致している場合YES
 */
BOOL	sk4IsEqualString( NSString* str1, NSString* str2 );


/**
 *  大文字／小文字を無視して文字列を比較（nil対応）
 *
 *  @param str1 文字列1
 *  @param str2 文字列2
 *
 *  @return 一致している場合YES
 */
BOOL	sk4IsEqualStringIgnoreCase( NSString* str1, NSString* str2 );


/**
 *  配列の中から、文字列として比較して完全に一致したものを探す関数
 *
 *  @param ar  配列
 *  @param str 文字列
 *
 *  @return 一致したものがある場合はindex。無かった場合は-1
 */
int		sk4SearchString( NSArray* ar,NSString* str );


/**
 *  文字列の前後の空白を取り除く
 *
 *  @param str 元の文字列
 *
 *  @return 空白を取り除いた文字列
 */
NSString*	sk4TrimString( NSString* str );


/**
 *  ローカライズされた文字列を取得するための関数
 *
 *  @param key キーになる文字列
 *
 *  @return ローカライズされた文字列が見つかった場合はそれ。
 *  		見つからなかった場合はキーで指定された文字列
 */
NSString*	sk4L9Str( NSString* key );


////////////////////////////////////////////////////////////////
/// @name	構造体関係

/**
 *  二つのfloatの差が、一定の範囲内かどうかを判定する関数
 *
 *  @param c0  判定に使うfloat
 *  @param c1  判定に使うfloat
 *  @param dif 許される差。0を指定した場合、完全に一致するかどうかを判定
 *
 *  @return 範囲内の場合YES
 */
BOOL		sk4IsRangeFloat( CGFloat c0,CGFloat c1,CGFloat dif );


/**
 *  CGRectの中心を取得
 *
 *  @param re CGRect
 *
 *  @return 中心
 */
CGPoint		sk4GetRectCenter( CGRect re );


/**
 *  CGRectの中心を設定
 *
 *  @param re  CGRectへのポインター
 *  @param pos 中心
 */
void		sk4SetRectCenter( CGRect* re, CGPoint pos );


/**
 *  CGSizeの小さい方の辺を取得
 *
 *  @param si CGSize
 *
 *  @return 小さい方の辺
 */
CGFloat		sk4GetSmallEdge( CGSize si );


/**
 *  ステータスバーの高さを取得
 *
 *  @return 高さ
 */
CGFloat		sk4GetStatusBarHeight( void );


////////////////////////////////////////////////////////////////
/// @name	その他の関数

/**
 *  nilをNULLに変換する
 *
 *  @param obj 元のオブジェクト
 *
 *  @return 変換後のオブジェクト
 */
id		sk4NilToNull( id obj );


/**
 *  NULLをnilに変換する
 *
 *  TODO: 1つにまとめるべきか？
 *
 *  @param obj 元のオブジェクト
 *
 *  @return 変換後のオブジェクト
 */
id		sk4NullToNil( id obj );


/**
 *  NSURLの配列をNSStringの配列に変換する
 *
 *  @param ar NSURLの配列
 *
 *  @return NSStringの配列
 */
NSMutableArray* sk4UrlToString( NSArray* ar );


/**
 *  単色で塗りつぶしたイメージを作成する
 *
 *  @param size イメージのサイズ
 *  @param r    red
 *  @param g    green
 *  @param b    blue
 *  @param a    alpha
 *
 *  @return イメージ
 */
NSImage* sk4ColorFillImage( CGSize size, CGFloat r, CGFloat g, CGFloat b, CGFloat a );


/**
 *  透明色の表示に使われる市松模様のイメージを作成する
 *
 *  @param size サイズ
 *
 *  @return イメージ
 */
NSImage* sk4TransparentPatternImage( CGSize size );


/**
 *  イメージの背景を透明色の市松模様にする
 *
 *  @param image 元々のイメージ
 *
 *  @return 背景を市松模様にしたイメージ
 */
NSImage* sk4TransparentBackground( NSImage* image );


/**
 *  NSImageからピクセル数を取得
 *
 *  @param image イメージ
 *
 *  @return ピクセル数
 */
CGSize	sk4ImagePixelSize( NSImage* image );


/**
 *  警告メッセージを表示する
 *
 *  @param mes メッセージ
 */
void	sk4WarningAlert( NSString* mes );


////////////////////////////////////////////////////////////////
/// @name	inline関係

/**
 *  単純な線形補間関数
 *
 *  元の終点と終点が同じ場合、補間後の中間点を返す。
 *
 *  @param v1 補間後の始点
 *  @param v2 補間後の終点
 *  @param t1 元の始点
 *  @param t2 元の終点
 *  @param ti 元になる値
 *
 *  @return 補間後の値
 *
 *  ex) iが0からno-1まで変化する間に、0.0からre.size.heightまで変化する値を取得
 *    for( int i=0; i<no; i++ ) {
 *        ce.y = sk4Smooth( 0.0, re.size.height, 0, no-1, i );
 *        [self drawCircleOne:ce radius:mr];
 *    }
 *    
 */
static inline
double		sk4Smooth( double v1,double v2,double t1,double t2,double ti )
{
	//		単純な線形補間
	
	if( t1 == t2 )
		return	( v1 + v2 ) / 2.0;
	else
		return	( v1 * ( t2 - ti ) + v2 * ( ti - t1 ) ) / ( t2 - t1 );
}


/**
 *  角度（度数）をラジアンに変換する関数
 *
 *  @param deg 角度
 *
 *  @return ラジアン
 */
static inline
CGFloat sk4DegreeToRadian( CGFloat deg )
{
	//		角度をラジアンに変換
	
	return deg * M_PI / 180.0;
}


/**
 *  ラジアンを角度（度数）に変換
 *
 *  @param rad ラジアン
 *
 *  @return 角度
 */
static inline
CGFloat sk4RadianToDegree( CGFloat rad )
{
	//		ラジアンを角度に変換
	
	return rad * 180.0 / M_PI;
}
	
	
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif /* defined(__SK4_X_LIB__) */

//	eof
