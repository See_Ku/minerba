//
//  SK4BoolStringTransformer.h
//  	for sk4lib
//
//  Created by See.Ku on 2014/03/29.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import <Foundation/Foundation.h>

#import "sk4xlib.h"

SK4_DEFINE_EXTERN( SK4_BOOL_STRING_TRANSFORMER );

/**
 *  BOOLをNSStringに変換するValueTransformer
 */
@interface SK4BoolStringTransformer : NSValueTransformer

/**
 *  変換後のクラスを返す
 *
 *  @return 変換後のクラス
 */
+ (Class)transformedValueClass;


/**
 *  逆変換を許可
 *
 *  @return YES: 逆変換を許可
 */
+ (BOOL)allowsReverseTransformation;


/**
 *  SK4BoolStringTransformerをシステムに登録
 */
+ (void)registTransformer;

@end
