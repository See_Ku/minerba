//
//  SK4MessageView.h
//  	for sk4lib
//
//  Created by See.Ku on 2014/03/19.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import <Cocoa/Cocoa.h>

/**
 *  半透明の背景にメッセージを表示するView
 */
@interface SK4MessageView : NSView

@property (nonatomic) NSString* message;

@end
