//
//  sk4xlib.m
//
//
//  Created by See.Ku on 2014/03/17.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import "sk4xlib.h"

////////////////////////////////////////////////////////////////
//		文字列関係

BOOL	sk4IsEmptyString( NSString* str )
{
	//		文字列は空か？
	
	if( str == nil )
		return YES;
	
	if( str.length == 0 )
		return YES;
	
	return NO;
}

BOOL	sk4IsEqualString( NSString* str1, NSString* str2 )
{
	//		文字列を比較（nil対応）
	
	//		先にnilをチェック
	
	if( str1 == nil && str2 == nil )
		return TRUE;
	
	if( str1 == nil || str2 == nil )
		return FALSE;
	
	//		文字列を比較
	
	if( [str1 compare:str2] == NSOrderedSame )
		return YES;
	
	return NO;
}

BOOL	sk4IsEqualStringIgnoreCase( NSString* str1, NSString* str2 )
{
	//		大文字／小文字を無視して文字列を比較（nil対応）
	
	//		先にnilをチェック
	
	if( str1 == nil && str2 == nil )
		return TRUE;
	
	if( str1 == nil || str2 == nil )
		return FALSE;
	
	//		大文字／小文字を無視して文字列を比較
	
	if( [str1 compare:str2 options:NSCaseInsensitiveSearch] == NSOrderedSame )
		return YES;
	
	return NO;
}

int		sk4SearchString( NSArray* ar,NSString* str )
{
	//		単純な文字列の検索
	
	int max = (int)ar.count;
	for( int i=0; i<max; i++ ) {
		if( [ar[i] isEqualToString:str] )
			return i;
	}
	
	return -1;
}

NSString*	sk4TrimString( NSString* str )
{
	//		文字列の前後の空白を取り除く
	
	return [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

NSString*	sk4L9Str( NSString* key )
{
	//		ローカライズされた文字列を取得する
	
	return [[NSBundle mainBundle] localizedStringForKey:(key) value:key table:nil];
}


////////////////////////////////////////////////////////////////
//		構造体関係

BOOL		sk4IsRangeFloat( CGFloat c0,CGFloat c1,CGFloat dif )
{
	//		値が範囲内にあるか？
	
	CGFloat tmp = c0 - c1;
	
	if( -dif <= tmp && tmp <= dif )
		return TRUE;
	
	return FALSE;
}

CGPoint		sk4GetRectCenter( CGRect re )
{
	//		CGRectの中心を取得
	
	CGPoint pos;
	pos.x = re.origin.x + re.size.width/2;
	pos.y = re.origin.y + re.size.height/2;
	
	return pos;
}

void		sk4SetRectCenter( CGRect* re, CGPoint pos )
{
	//		CGRectの中心を設定
	
	re->origin.x = pos.x - re->size.width/2;
	re->origin.y = pos.y - re->size.height/2;
}

CGFloat		sk4GetSmallEdge( CGSize si )
{
	//		CGSizeの小さい方の辺を取得
	
	if( si.width < si.height )
		return si.width;
	else
		return si.height;
}


////////////////////////////////////////////////////////////////
//		その他

id		sk4NilToNull( id obj )
{
	//		nilをNSNullに変換
	
	if( obj == nil )
		return [NSNull null];
	else
		return obj;
}

id		sk4NullToNil( id obj )
{
	//		NSNullをnilに変換
	
	if( obj == [NSNull null] )
		return nil;
	else
		return obj;
}


NSMutableArray* sk4UrlToString( NSArray* ar )
{
	//		NSURLの配列をNSStringの配列に変換する
	
	NSMutableArray* new_ar = [[NSMutableArray alloc] initWithCapacity:ar.count];
	
	for( NSURL* url in ar ) {
		NSString* str = [url path];
		
		[new_ar addObject:str];
	}
	
	return new_ar;
}

NSImage* sk4ColorFillImage( CGSize size, CGFloat r, CGFloat g, CGFloat b, CGFloat a )
{
	//		単色で塗りつぶしたイメージを作成する

	NSImage* img = [[NSImage alloc] initWithSize:size];
	
	[img lockFocus];
	[NSGraphicsContext saveGraphicsState];
	
	CGContextRef cgcr = [[NSGraphicsContext currentContext] graphicsPort];
	CGContextSetRGBFillColor( cgcr, r, g, b, a );
	
	CGRect re = CGRectMake( 0, 0, size.width, size.height );
	CGContextFillRect( cgcr, re );
	
	[NSGraphicsContext restoreGraphicsState];
	[img unlockFocus];
	
	return img;
}

NSImage* sk4TransparentPatternImage( CGSize size )
{
	//		透明色の表示に使われる市松模様のイメージを作成する
	
	//		描画に必要な情報を予め計算
	
	const CGFloat unit = 8.0;
	const CGFloat dark = 0.749019608;
	const CGFloat light = 1.0;
	
	NSInteger cx = size.width / 2;
	NSInteger nx = ( cx + unit - 1 ) / unit;
	
	NSInteger cy = size.height / 2;
	NSInteger ny = ( cy + unit - 1 ) / unit;
	
	//		描画先を適当に作成
	
	NSImage* img = [[NSImage alloc] initWithSize:size];
	
	//		描画開始
	
	[img lockFocus];
	[NSGraphicsContext saveGraphicsState];
	
	CGContextRef cgcr = [[NSGraphicsContext currentContext] graphicsPort];
	
	//		背景を塗りつぶし
	
	CGRect re;
	
	CGContextSetRGBFillColor( cgcr, light, light, light, 1 );
	re = CGRectMake( 0, 0, size.width, size.height );
	CGContextFillRect( cgcr, re );
	
	//		市松模様を描画
	
	CGContextSetRGBFillColor( cgcr, dark, dark, dark, 1 );
	re = CGRectMake( 0, 0, unit, unit );
	
	NSInteger no = 0;
	
	for( NSInteger x=-nx; x<nx; x++, no++ ) {
		for( NSInteger y=-ny; y<ny; y++, no++ ) {
			if( no % 2 )
				continue;
			
			re.origin.x = cx + x * unit;
			re.origin.y = cy + y * unit;
			
			CGContextFillRect( cgcr, re );
		}
	}
	
	//		描画終了
	
	[NSGraphicsContext restoreGraphicsState];
	[img unlockFocus];
	
	return img;
}

NSImage*	sk4TransparentBackground( NSImage* image )
{
	//		イメージの背景を透明色の市松模様にする

	CGSize size = sk4ImagePixelSize( image );
	NSImage* back = sk4TransparentPatternImage( size );

	CGRect re;
	re.origin = CGPointZero;
	re.size = size;

	[back lockFocus];
	[NSGraphicsContext saveGraphicsState];

	[image drawInRect:re fromRect:CGRectZero operation:NSCompositeSourceOver fraction:1.0];

	[NSGraphicsContext restoreGraphicsState];
	[back unlockFocus];

	return back;
}

CGSize	sk4ImagePixelSize( NSImage* image )
{
	//		NSImageからピクセル数を取得

	CGSize size = image.size;

	NSArray* ar = image.representations;
	if( ar.count == 0 )
		return size;

	NSImageRep* rep = ar[0];
	if( [rep isKindOfClass:[NSImageRep class]] ) {
		size.width = rep.pixelsWide;
		size.height = rep.pixelsHigh;
	}

	return size;
}


void	sk4WarningAlert( NSString* mes )
{
	//		警告メッセージを表示する

	NSAlert* al = [NSAlert alertWithMessageText:mes defaultButton:nil alternateButton:nil otherButton:nil informativeTextWithFormat:@""];

	NSApplication* app = [NSApplication sharedApplication];
	NSWindow* win = app.mainWindow;

	[al beginSheetModalForWindow:win completionHandler:nil];
}


//	eof
