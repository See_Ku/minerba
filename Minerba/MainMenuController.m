//
//  MainMenuController.m
//  Minerba
//
//  Created by See.Ku on 2014/03/28.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import "sk4xlib.h"
#import "SK4BitmapContext.h"

#import "AppDelegate.h"
#import "MainMenuController.h"
#import "DescriptionView.h"
#import "DrawDescription.h"

#import "AppIconController.h"
#import "BarCutController.h"


@interface MainMenuController ()
{
	AppIconController* _iosAppIcon;
	AppIconController* _osxAppIcon;
	BarCutController* _barCut;
}
@end

@implementation MainMenuController

- (void)awakeFromNib
{
	//		初期化

	//		プログラムの終了に対応

	NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
	[nc addObserver:self selector:@selector(appWillTerminate) name:NSApplicationWillTerminateNotification object:nil];

	//		説明文を設定
	DrawDescription* dd = _desc.descript;

	dd.title = @"Minerba";
	dd.titleColor = [NSColor blackColor];
	dd.detail = @"Image editing tool for Developer";
	[dd makeTextAtr];

	//		それぞれのボタンを設定

	[self makeButton];

	//		設定を読み込み

	[self defaultSettings];
	[self readSettings];
}

- (void)appWillTerminate
{
	[self writeSettings];
}

////////////////////////////////////////////////////////////////

- (void)makeButton
{
	[self makeButtonImage:_iosAppIconButton title:@"App Icon" detail:@"App Icon/Spotlight\nSettings/App Store"];
	[self makeButtonImage:_statusBarButton title:@"Cut StatusBar" detail:@"Cut StatusBar from screen shot."];

	[self makeButtonImage:_osxAppIconButton title:@"App Icon" detail:@"App Icon Only"];
}

- (void)makeButtonImage:(NSButton*)button title:(NSString*)title detail:(NSString*)detail
{
	CGRect re = button.bounds;

	DrawDescription* desc = [[DrawDescription alloc] init];
	desc.frame = re;
	desc.title = title;
	desc.detail = detail;
	[desc makeTextAtr];

	NSImage* img = [[NSImage alloc] initWithSize:re.size];

	[img lockFocus];
	[NSGraphicsContext saveGraphicsState];

	CGContextRef cg = [[NSGraphicsContext currentContext] graphicsPort];
	[desc draw:cg];

	[NSGraphicsContext restoreGraphicsState];
	[img unlockFocus];

	button.image = img;
}


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

#pragma mark - アクション

- (IBAction)makeIosAppIcon:(id)sender
{
	[self openAppIcon:YES ios:YES];
}

- (IBAction)makeOsxAppIcon:(id)sender
{
	[self openAppIcon:YES ios:NO];
}

- (void)openAppIcon:(BOOL)new ios:(BOOL)ios
{
	AppIconController* aic = _iosAppIcon;
	if( ios == NO )
		aic = _osxAppIcon;

	if( aic == nil ) {
		aic = [[AppIconController alloc] initWithFlag:ios];
		if( ios )
			_iosAppIcon = aic;
		else
			_osxAppIcon = aic;

	} else {
		if( aic.window.isVisible )
			return;
	}

	if( new )
		[aic resetInfo];

	[aic showWindow];
}

- (IBAction)cutStatusBar:(id)sender
{
	[self openCutBar:YES];
}

- (void)openCutBar:(BOOL)new
{
	if( _barCut == nil ) {
		_barCut = [[BarCutController alloc] init];
	} else {
		if( _barCut.window.isVisible )
			return;
	}

	if( new )
		[_barCut resetInfo];

	[_barCut showWindow];
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

#pragma mark - ユーザーデフォルト関係

SK4_DEFINE_STATIC( MAIN_APP_ICON_IOS );
SK4_DEFINE_STATIC( MAIN_APP_ICON_OSX );
SK4_DEFINE_STATIC( MAIN_BAR_CUT );

- (void)defaultSettings
{
	//		デフォルトを設定

	NSMutableDictionary* dic = [[NSMutableDictionary alloc] init];

	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	[ud registerDefaults:dic];
}

- (void)readSettings
{
	//		ユーザーデフォルトを読み込み

	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];

	if( [ud boolForKey:MAIN_APP_ICON_IOS] )
		[self openAppIcon:NO ios:YES];

	if( [ud boolForKey:MAIN_APP_ICON_OSX] )
		[self openAppIcon:NO ios:NO];

	if( [ud boolForKey:MAIN_BAR_CUT] )
		[self openCutBar:NO];
}

- (void)writeSettings
{
	//		ユーザーデフォルトを保存

	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];

	BOOL flag = FALSE;

	flag = _iosAppIcon.window.isVisible;
	[ud setObject:@(flag) forKey:MAIN_APP_ICON_IOS];

	flag = _osxAppIcon.window.isVisible;
	[ud setObject:@(flag) forKey:MAIN_APP_ICON_OSX];

	flag = _barCut.window.isVisible;
	[ud setObject:@(flag) forKey:MAIN_BAR_CUT];

	[ud synchronize];
}

@end

// eof
