//
//  BarCutInfo.m
//  Minerba
//
//  Created by See.Ku on 2014/04/03.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import "BarCutInfo.h"

@interface BarCutInfo ()
{
	NSArray* _infoArray;
}
@end

@implementation BarCutInfo

- (NSArray*)infoArray
{
	if( _infoArray != nil )
		return _infoArray;

	NSString* path = [[NSBundle mainBundle] pathForResource:@"barcutinfo" ofType:@"plist"];
	NSDictionary* dic = [NSDictionary dictionaryWithContentsOfFile:path];

	_infoArray = dic[@"screenInfo"];

	return _infoArray;
}

- (NSDictionary*)sizeToInfo:(CGSize)size
{
	for( NSDictionary* dic in self.infoArray ) {
		NSInteger wx = [dic[@"width"] integerValue];
		NSInteger wy = [dic[@"height"] integerValue];

		if( wx == size.width && wy == size.height )
			return dic;
	}

	return nil;
}

////////////////////////////////////////////////////////////////

- (NSString*)infoString:(CGSize)size
{
	NSDictionary* dic = [self sizeToInfo:size];
	if( dic != nil )
		return dic[@"info"];

	return nil;
}

- (NSInteger)cutSize:(CGSize)size
{
	NSDictionary* dic = [self sizeToInfo:size];
	if( dic != nil )
		return [dic[@"cut"] integerValue];

	return 0;
}

@end
