//
//  DescriptionView.m
//  Minerba
//
//  Created by See.Ku on 2014/03/28.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import "DescriptionView.h"
#import "DrawDescription.h"

@implementation DescriptionView

- (id)initWithFrame:(NSRect)frame
{
	self = [super initWithFrame:frame];
	if( self == nil )
		return nil;

	_descript = [[DrawDescription alloc] initWithFrame:frame];

    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
	[super drawRect:dirtyRect];

	CGContextRef cg = [[NSGraphicsContext currentContext] graphicsPort];
	[_descript draw:cg];
}

@end
