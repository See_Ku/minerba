//
//  AppIconController.h
//  Minerba
//
//  Created by See.Ku on 2014/03/29.
//  Copyright (c) 2014 AxeRoad All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SK4DropFileView.h"

@class SK4MessageView;

@interface BasicConvertController : NSObject <NSTableViewDelegate, SK4DropFileViewDelegate>

////////////////////////////////////////////////////////////////
//		初期化関係

@property (nonatomic) NSString* settingsKey;
@property (nonatomic) NSArray* inputArray;
@property (strong) IBOutlet NSWindow *window;
@property (weak) IBOutlet SK4MessageView *messageView;

- (void)resetInfo;
- (void)showWindow;

- (NSString*)makeKey:(NSString*)base;
- (void)addTableColumn:(NSString*)title width:(NSInteger)width path:(NSString*)path options:(NSDictionary*)opt;
- (NSTableColumn*)makeTableColumn:(NSString*)title width:(NSInteger)width;
- (NSTableColumn*)makeTableColumn:(NSString*)title width:(NSInteger)width align:(NSTextAlignment)align editable:(BOOL)editable;

////////////////////////////////////////////////////////////////
//		入力関係

@property (weak) IBOutlet SK4DropFileView *dropView;
@property (weak) IBOutlet NSTextField *inputLabel;
@property (weak) IBOutlet NSTableView *tableView;
@property (strong) IBOutlet NSArrayController *arrayController;
@property (weak) IBOutlet NSTextField *typeLabel;
@property (weak) IBOutlet NSComboBox *typeCombo;

- (IBAction)changeTypeCombo:(NSComboBox *)sender;

////////////////////////////////////////////////////////////////
//		ドロワー関係

@property (strong) IBOutlet NSDrawer *drawer;
@property (weak) IBOutlet NSButton *flipDrawerButton;

- (IBAction)flipDrawer:(id)sender;
- (NSImageView*)drawerImageView;

////////////////////////////////////////////////////////////////
//		出力関係

@property (weak) IBOutlet NSTextField *outputDir;

- (IBAction)selectOutputDir:(id)sender;
- (IBAction)convertExec:(id)sender;

@end
